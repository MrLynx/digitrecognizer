import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *

from gui import layout
from perceptron.network import Network


class TrainingThread(QThread):
    def __init__(self):
        QThread.__init__(self)

    def __del__(self):
        self.wait()

    def set_network(self, network, network_properties):
        self.network = network
        self.network_properties = network_properties

    def run(self):
        training_time, test_efficiency = self.network.train(*self.network_properties)
        self.emit(SIGNAL('finishedTraining(int,double)'), training_time, test_efficiency)


class MainWindow(QMainWindow, layout.Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)

        def on_regularization_checkbox_changed():
            if self.regularizationCheckBox.checkState() == Qt.Unchecked:
                self.regularizationEnabled = False
                self.regularizationValueDoubleSpinBox.setEnabled(False)
                self.regularizationValueLabel.setEnabled(False)
            else:
                self.regularizationEnabled = True
                self.regularizationValueDoubleSpinBox.setEnabled(True)
                self.regularizationValueLabel.setEnabled(True)

        def on_regularization_value_changed():
            self.regularization = self.regularizationValueDoubleSpinBox.value()

        def on_hidden_layers_num_changed():
            self.layers = [28 * 28]
            for (i, (label, spin_box)) in enumerate(self.layers_spin_boxes):
                if i + 1 <= self.hiddenLayersSpinBox.value():
                    spin_box.setEnabled(True)
                    label.setEnabled(True)
                    self.layers += [spin_box.value()]
                else:
                    spin_box.setEnabled(False)
                    label.setEnabled(False)

            self.layers += [10]
            self.layers = tuple(self.layers)

        def on_hidden_layer_value_changed():
            self.layers = [28 * 28]
            for i in xrange(self.hiddenLayersSpinBox.value()):
                if self.layers_spin_boxes[i][0].isEnabled():
                    self.layers += [int(self.layers_spin_boxes[i][1].value())]
            self.layers += [10]
            self.layers = tuple(self.layers)

        def on_create_network_button_push():
            if self.regularizationEnabled:
                self.network = Network(layers_sizes=self.layers, regularization=self.regularization)
            else:
                self.network = Network(layers_sizes=self.layers)

            self.connect(self.network, SIGNAL('setProgress(int)'), self.set_progress)

            self.trainNetworkButton.setEnabled(True)
            self.statusBar().showMessage('Created network.')

        def on_train_network_button_push():
            self.statusBar().showMessage('Training network...')

            if self.verticalLayout_7.count() > 0:
                for i in reversed(range(self.verticalLayout_7.count())):
                    self.verticalLayout_7.itemAt(i).widget().setParent(None)

            self.verticalLayout_7.addWidget(self.network.plot_efficiency)
            self.verticalLayout_7.addWidget(self.network.plot_cost)

            self.createNetworkButton.setEnabled(False)
            self.trainNetworkButton.setEnabled(False)

            batch_size = self.miniBatchSizeSpinBox.value()
            learning_rate = self.learningRateDoubleSpinBox.value()
            epochs = self.epochsSpinBox.value()
            device = str(self.deviceComboBox.currentText()).lower()
            train_set_size = self.learningSetSizeSpinBox.value()
            self.recognizeButton.setEnabled(False)

            training_args = [batch_size, learning_rate, epochs, device, train_set_size, self]
            self.trainingThread.set_network(self.network, training_args)
            self.trainingThread.start()

        def on_training_finished(training_time, test_efficiency):
            hours = (training_time / 3600)
            minutes = (training_time % 3600) / 60
            seconds = training_time % 60

            self.statusBar().showMessage(
                'Training network finished. Test set error: %.2f%% Training time: %dh-%dm-%ds' % (
                    100. - test_efficiency, hours, minutes, seconds))

            self.trainingThread.terminate()
            self.createNetworkButton.setEnabled(True)
            self.recognizeButton.setEnabled(True)

        def on_recognize_button_pushed():

            x, icon = self.imageWidget.get_image_array()
            y = self.network.get_prediction(x)

            item = QListWidgetItem(str(y))
            item.setIcon(icon)
            self.resultsList.insertItem(0, item)

        # INIT

        self.trainingThread = TrainingThread()
        self.network = None

        self.layers_spin_boxes = []
        self.layers_spin_boxes.append((self.hiddenLayer1Label, self.hiddenLayer1SpinBox))
        self.layers_spin_boxes.append((self.hiddenLayer2Label, self.hiddenLayer2SpinBox))
        self.layers_spin_boxes.append((self.hiddenLayer3Label, self.hiddenLayer3SpinBox))
        self.layers_spin_boxes.append((self.hiddenLayer4Label, self.hiddenLayer4SpinBox))
        self.layers_spin_boxes.append((self.hiddenLayer5Label, self.hiddenLayer5SpinBox))
        on_hidden_layers_num_changed()

        self.regularizationCheckBox.setCheckState(Qt.Unchecked)
        self.regularizationValueDoubleSpinBox.setEnabled(False)

        self.regularizationEnabled = False
        self.regularization = 0.0

        self.connect(self.regularizationCheckBox, SIGNAL('stateChanged(int)'), on_regularization_checkbox_changed)
        self.connect(self.regularizationValueDoubleSpinBox, SIGNAL('valueChanged(double)'),
                     on_regularization_value_changed)
        self.connect(self.hiddenLayersSpinBox, SIGNAL('valueChanged(int)'), on_hidden_layers_num_changed)
        for (_, spinBox) in self.layers_spin_boxes:
            self.connect(spinBox, SIGNAL('valueChanged(int)'), on_hidden_layer_value_changed)

        self.connect(self.createNetworkButton, SIGNAL('clicked()'), on_create_network_button_push)
        self.connect(self.trainNetworkButton, SIGNAL('clicked()'), on_train_network_button_push)
        self.connect(self.clearImageButton, SIGNAL('clicked()'), self.imageWidget.clearImage)
        self.connect(self.recognizeButton, SIGNAL('clicked()'), on_recognize_button_pushed)
        self.connect(self.penWidthSpinBox, SIGNAL('valueChanged(int)'), (lambda w: self.imageWidget.set_pen_width(w)))

        self.connect(self.trainingThread, SIGNAL('finishedTraining(int,double)'), on_training_finished)

    def set_progress(self, progress):
        self.progressBar.setValue(progress)
        self.progressBar_2.setValue(progress)

    def closeEvent(self, *args, **kwargs):
        self.trainingThread.terminate()


app = QApplication(sys.argv)
form = MainWindow()
form.show()
app.exec_()
