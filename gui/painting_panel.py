from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import QImage, QIcon
import numpy as np
from PIL import Image


class ScribbleArea(QtGui.QWidget):
    def __init__(self, parent=None):
        super(ScribbleArea, self).__init__(parent)
        self.setMaximumSize(2 * 28, 2 * 28)
        self.setMinimumSize(2 * 28, 2 * 28)

        self.setAttribute(QtCore.Qt.WA_StaticContents)
        self.modified = False
        self.scribbling = False
        self.myPenWidth = 7
        self.myPenColor = QtCore.Qt.white
        self.image = QtGui.QImage(QtCore.QSize(2 * 28, 2 * 28), QtGui.QImage.Format_RGB32)
        self.lastPoint = QtCore.QPoint()

    def get_image_array(self, dtype='array'):
        qimage = self.image.copy()

        bgra_dtype = np.dtype({'b': (np.uint8, 0),
                              'g': (np.uint8, 1),
                              'r': (np.uint8, 2),
                              'a': (np.uint8, 3)})

        result_shape = (qimage.height(), qimage.width())
        temp_shape = (qimage.height(),
                      qimage.bytesPerLine() * 8 / qimage.depth())
        if qimage.format() in (QImage.Format_ARGB32_Premultiplied,
                               QImage.Format_ARGB32,
                               QImage.Format_RGB32):
            if dtype == 'rec':
                dtype = bgra_dtype
            elif dtype == 'array':
                dtype = np.uint8
                result_shape += (4, )
                temp_shape += (4, )
        elif qimage.format() == QImage.Format_Indexed8:
            dtype = np.uint8

        buf = qimage.bits().asstring(qimage.numBytes())
        result = np.frombuffer(buf, dtype).reshape(temp_shape)
        if result_shape != temp_shape:
            result = result[:,:result_shape[1]]
        if qimage.format() == QImage.Format_RGB32 and dtype == np.uint8:
            result = result[...,:3]

        img = Image.fromarray(result)
        img = img.resize((28, 28))
        img = img.convert('L')

        t = np.array(img.getdata(), np.uint8)
        t = 1.0 * t / 255.0

        img.thumbnail((28, 28), Image.ANTIALIAS)
        pixmap = QtGui.QPixmap(28, 28)
        pixmap.convertFromImage(self.image)

        return t.astype('float32'), QIcon(pixmap)

    def clearImage(self):
        self.image.fill(QtGui.qRgb(0, 0, 0))
        self.modified = True
        self.update()

    def set_pen_width(self, width):
        self.myPenWidth = width

    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            self.lastPoint = event.pos()
            self.scribbling = True

    def mouseMoveEvent(self, event):
        if (event.buttons() & QtCore.Qt.LeftButton) and self.scribbling:
            self.drawLineTo(event.pos())

    def mouseReleaseEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton and self.scribbling:
            self.drawLineTo(event.pos())
            self.scribbling = False

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        painter.drawImage(event.rect(), self.image)

    def resizeEvent(self, event):
        self.resizeImage(self.image, event.size())
        super(ScribbleArea, self).resizeEvent(event)

    def drawLineTo(self, endPoint):

        painter = QtGui.QPainter(self.image)
        painter.setRenderHint(QtGui.QPainter.Antialiasing)
        painter.setPen(QtGui.QPen(self.myPenColor, self.myPenWidth,
            QtCore.Qt.SolidLine, QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin))

        painter.drawLine(self.lastPoint, endPoint)
        self.modified = True

        self.update()
        self.lastPoint = QtCore.QPoint(endPoint)

    def resizeImage(self, image, new_size):
        new_image = QtGui.QImage(new_size, QtGui.QImage.Format_RGB32)
        new_image.fill(QtGui.qRgb(255, 255, 255))
        painter = QtGui.QPainter(new_image)
        painter.drawImage(QtCore.QPoint(0, 0), image)
        self.image = new_image