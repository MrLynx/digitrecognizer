import theano.tensor as T
import theano
import numpy as np


class Layer:
    def __init__(self, number_generator, input, input_size, output_size, activation_function):
        self.input = input
        self.activation_function = activation_function

        # Randomowa inicjalizacja wag

        W_values = np.asarray(number_generator.uniform(
            low=-np.sqrt(6. / (input_size + output_size)),
            high=np.sqrt(6. / (input_size + output_size)),
            size=(input_size, output_size)),
                              dtype=theano.config.floatX)

        if activation_function == T.nnet.sigmoid:
            W_values *= 4

        b_values = np.zeros((output_size,), dtype=theano.config.floatX)

        # Zdefiniowanie, jak obliczyc wyjscie z warstwy

        self.W = theano.shared(value=W_values, name='W', borrow=True)
        self.b = theano.shared(value=b_values, name='b', borrow=True)

        self.output = activation_function(T.dot(input, self.W) + self.b)

        self.L = abs(self.W).sum()

        self.params = [self.W, self.b]