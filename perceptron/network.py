import sys
import time

import theano.tensor as T
import theano
import numpy as np
from PyQt4.QtCore import *

from gui.matplotlib_widget import MatplotlibCanvas

from perceptron import mnist_loader
from perceptron.layers import Layer


class Network(QThread):
    def __init__(self, number_generator=np.random.RandomState(1234), layers_sizes=None, plotter=None, regularization=-1):
        QThread.__init__(self)

        self.prepare_plots()


        theano.config.floatX = 'float32'
        x = T.matrix('x')
        self.input = x
        self.regularization = regularization
        self.layers = []

        for i in xrange(1, len(layers_sizes)):
            if i == 1:
                input = self.input
            else:
                input = self.layers[-1].output

            if i == len(layers_sizes) - 1:
                activation = T.nnet.softmax
            else:
                activation = T.tanh

            self.layers.append(Layer(number_generator, input, layers_sizes[i - 1], layers_sizes[i], activation))

        self.probabilities = self.layers[-1].output
        self.prediction = T.argmax(self.probabilities, axis=1)

        self.L = 0
        self.params = []
        for layer in self.layers:
            self.L += (layer.W ** 2).sum()
            self.params += layer.params

    def get_prediction(self, inp):

        inp = np.reshape(inp, (1, len(inp)))
        v = T.matrix('v')
        get_output = theano.function(inputs=[v], outputs=self.prediction, givens={self.input: v})

        return get_output(inp)[0]

    def errors(self, y):
        return (1. - T.mean(T.neq(self.prediction, y))) * 100.

    def train(self, batch_size=600, learning_rate=0.2, epochs=100, device='gpu', train_set_size=60000, parent=None):

        start_time = time.time()

        import theano.sandbox.cuda

        theano.sandbox.cuda.use(device)

        self.epochs = epochs

        print 'Loading data...'

        train_set, validation_set, test_set = mnist_loader.load_data()
        train_set = train_set[:train_set_size]

        train_X = theano.shared(np.asarray(train_set[0], dtype=theano.config.floatX))
        train_Y = T.cast(theano.shared(np.asarray(train_set[1], dtype=theano.config.floatX)), 'int32')

        valid_X = theano.shared(np.asarray(validation_set[0], dtype=theano.config.floatX))
        valid_Y = T.cast(theano.shared(np.asarray(validation_set[1], dtype=theano.config.floatX)), 'int32')

        test_X = theano.shared(np.asarray(test_set[0], dtype=theano.config.floatX))
        test_Y = T.cast(theano.shared(np.asarray(test_set[1], dtype=theano.config.floatX)), 'int32')

        print 'Preparing functions to train...'

        index = T.lscalar('index')
        y = T.ivector('y')

        if self.regularization < 0:
            cost = -T.mean(T.log(self.probabilities)[T.arange(y.shape[0]), y])
        else:
            cost = -T.mean(T.log(self.probabilities)[T.arange(y.shape[0]), y]) + self.regularization * self.L

        test_model = theano.function(inputs=[], outputs=self.errors(y), givens={self.input: test_X, y: test_Y})
        validate_model = theano.function(inputs=[], outputs=self.errors(y), givens={self.input: valid_X, y: valid_Y})

        gradients = []
        for param in self.params:
            g = T.grad(cost, param)
            gradients.append(g)

        updates = []
        for param, param_gradient in zip(self.params, gradients):
            updates.append((param, param - learning_rate * param_gradient))

        train_model = theano.function(inputs=[index],
                                      outputs=cost,
                                      updates=updates,
                                      givens={self.input: train_X[index * batch_size: (index + 1) * batch_size],
                                              y: train_Y[index * batch_size: (index + 1) * batch_size]})

        print 'Training...'

        train_batches_num = train_X.get_value(borrow=True).shape[0] / batch_size

        self.test_results = []
        self.valid_results = []

        self.costs = []

        for i in xrange(epochs):

            test_efficiency = test_model()
            valid_efficiency = validate_model()

            for j in xrange(train_batches_num):
                current_cost = train_model(j)

            self.test_results.append(test_efficiency)
            self.valid_results.append(valid_efficiency)
            self.costs.append(current_cost)

            progress = (100. * (i + 1) / epochs)
            sys.stdout.write('\rEpoch: %d. cost: %.5f Test score: %.4f%% Validation score: %.4f%% Progress: %.1f%%' % (
                i, current_cost, test_efficiency, valid_efficiency, progress))
            sys.stdout.flush()

            self.repaint_plots(epochs)

            training_time = time.time() - start_time

            if parent is not None:
                self.emit(SIGNAL('setProgress(int)'), int(progress))

        return int(training_time), test_efficiency

    def repaint_plots(self, epochs):
        self.plot_efficiency.axes.set_xlim([1, epochs + 1])
        self.plot_efficiency.axes.set_ylim([0, 100])
        self.plot_efficiency.axes.plot(range(1, len(self.test_results)+1), self.test_results, 'b-', label='Test set')
        self.plot_efficiency.axes.plot(range(1, len(self.test_results)+1), self.valid_results, 'r--', label='Validation set')
        self.plot_efficiency.draw()

        self.plot_cost.axes.set_xlim([1, epochs + 1])
        self.plot_cost.axes.set_ylim([0, self.costs[0]])
        self.plot_cost.axes.set_xlim([1, epochs + 1])
        self.plot_cost.axes.plot(range(1, len(self.costs)+1), self.costs, 'b-')
        self.plot_cost.draw()

    def prepare_plots(self):
        self.plot_efficiency = MatplotlibCanvas(title='Efficiency', xlabel='Epoch', ylabel='Efficiency [%]')
        self.plot_efficiency.axes.plot([0], [0], 'b-', label='Test set')
        self.plot_efficiency.axes.plot([0], [0], 'r--', label='Validation set')
        self.plot_efficiency.axes.legend(loc=4)

        self.plot_cost = MatplotlibCanvas(title='Cost', xlabel='Epoch', ylabel='Cost')
        self.plot_efficiency.axes.plot([0], [0], 'b-')